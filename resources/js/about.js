window.addEventListener('load', onLoad);
window.addEventListener('scroll', onScroll);

function onLoad() {

}

function onScroll() {
    parallaxAbout();
    setSkillWidth();
}

/**
 * Set all parallax effects to the about page
 */
function parallaxAbout() {
    let about = document.getElementById('about');
    let aboutBg = document.getElementById('about_background');
    let planets = document.getElementById('about_planets');
    if (about) {
        aboutBg.style.backgroundPositionY = (window.pageYOffset) / 1.2 + 'px'
        planets.style.top = -(window.pageYOffset) / 3 + 'px'
    }
}

/**
 * Add animations to the skill bars
 */
function setSkillWidth() {
    const skills = document.getElementsByClassName('skill__level__content');
    const skillContainerWidth = document.getElementById('skill_container').offsetWidth;

    Array.prototype.forEach.call(skills, function (skill) {
        const offset = skill.offsetTop - (window.innerHeight / 1.3);

        if (offset < window.pageYOffset) {
            // Set the bar width
            skill.style.width = skill.dataset.level + '%';
            skill.classList.add('skill__level__content--active');

            // Shoot the rocket to the right
            skill.getElementsByClassName('rocket')[0].style.right = skillContainerWidth * -1;
        }
    });
}
