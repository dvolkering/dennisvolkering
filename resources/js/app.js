window.onload = function () {
    toggleTopMenu(true);

    // Check if there is content above the fold
    showContents('paragraph');
    showContents('quote');
    showContents('image-text');
    showContents('contact-form');
    showContents('experience');


    document.getElementById('nav_toggle').onclick = function () {
        toggleMenu();
    }

    // Hide menu when clicking on the site part
    let site = document.getElementById('site');
    site.onclick = function () {
        if (site.classList.contains('site--active')) {
            toggleMenu();
        }
    }

    window.onscroll = function () {
        if (window.pageYOffset < 250) {
            toggleTopMenu(true);
        } else {
            toggleTopMenu(false);
        }

        parallaxBanner();
        showContents('paragraph');
        showContents('quote');
        showContents('image-text');
        showContents('contact-form');
        showContents('experience');

        blurImages();
    }
}

function blurImages() {
    let items = document.getElementsByClassName('image-text');

    Array.prototype.forEach.call(items, function (item) {
        const offset = item.offsetTop + (window.innerHeight / 2);
        const blur = ((window.pageYOffset - offset) / 100).toFixed(1);

        if (blur > 1) {
            console.log(blur);
            console.log(1 + ((blur - 1) / 100));

            item.getElementsByClassName('image-text__img__src')[0].style.filter = 'blur(' + (1 + ((blur - 1) / 3)) + 'px)';
            item.getElementsByClassName('image-text__img__src')[0].style.transform = 'scale(' + (1 + (blur / 100)) + ')';
        } else {
            item.getElementsByClassName('image-text__img__src')[0].style.filter = null;
            item.getElementsByClassName('image-text__img__src')[0].style.transform = null;
        }
    });

}

function showContents(block) {
    let items = document.getElementsByClassName(block);

    Array.prototype.forEach.call(items, function (item) {
        const offset = item.offsetTop - (window.innerHeight / 1.4);

        if (offset < window.pageYOffset) {
            item.classList.add(block + '--show');
        }
    });
}

function parallaxBanner() {
    let items = document.getElementsByClassName('banner--parallax');

    Array.prototype.forEach.call(items, function (item) {
        const content = item.getElementsByClassName('banner__content')[0];
        const titles = content.getElementsByClassName('banner__titles');
        const image = content.getElementsByClassName('banner__content__image__src')[0];
        const offset = item.offsetTop - window.innerHeight;
        const animation = window.pageYOffset - item.offsetTop;

        if (offset < window.pageYOffset) {
            image.style.objectPosition = (image.dataset.objectpositionX || 50) + '% ' + (50 + (animation / 30)) + '%';

            if (titles.length !== 0 && item.classList.contains('banner--titles-bar')) {
                titles[0].style.marginTop = animation / 2 + 'px';
            }
        }
    });
}


function toggleMenu() {
    document.getElementById('nav_toggle').classList.toggle('nav-toggle--active');
    document.getElementById('navigation').classList.toggle('menu--active');
    document.getElementById('site').classList.toggle('site--active');
    document.getElementById('footer').classList.toggle('footer--active');

}

function toggleTopMenu(show) {
    const navToggle = document.getElementById('nav_toggle').classList;
    const nav = document.getElementById('navigation').classList;
    const site = document.getElementById('site').classList;

    if (show && !site.contains('site--active')) {
        navToggle.remove('nav-toggle--show');
        // navToggle.add('nav-toggle--menu-is-on-top');
        nav.add('menu--ontop');
        site.add('site--menu-is-on-top');
    } else {
        navToggle.add('nav-toggle--show');
        nav.remove('menu--ontop');
        site.remove('site--menu-is-on-top');
    }
}
