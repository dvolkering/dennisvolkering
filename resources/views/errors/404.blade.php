@extends('site.layouts.app', [
    'meta_title' =>
        app('A17\Twill\Repositories\SettingRepository')->byKey('site_title') . ' | ' .
        app('A17\Twill\Repositories\SettingRepository')->byKey('404_title'),
    'meta_description' => app('A17\Twill\Repositories\SettingRepository')->byKey('site_description'),
    'meta_url' => Request::url(),
    ]
)

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <section class="page-not-found">
                    <div class="page-not-found__content">
                        <h1 class="title text-center mb-3">{{ app('A17\Twill\Repositories\SettingRepository')->byKey('404_title') }}</h1>
                        <h5 class="subtitle mb-2">{{ app('A17\Twill\Repositories\SettingRepository')->byKey('404_subtitle') }}</h5>
                        <p class="text">
                            {!! app('A17\Twill\Repositories\SettingRepository')->byKey('404_description')  !!}
                        </p>
                    </div>
                    <footer class="page-not-found__button text-center">
                        <a href="/" class="btn btn-primary btn-xl">{{ app('A17\Twill\Repositories\SettingRepository')->byKey('404_button_text') }}</a>
                    </footer>
                </section>
            </div>
        </div>
    </div>
@endsection

