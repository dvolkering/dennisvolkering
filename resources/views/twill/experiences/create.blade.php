@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => trans('experience.title'),
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])

@formField('input', [
    'name' => 'role',
    'label' => trans('experience.role'),
])

@formField('input', [
    'name' => 'description',
    'label' => trans('experience.description'),
    'maxlength' => 100
])

@formField('date_picker', [
    'name' => 'date_from',
    'label' => trans('experience.date_from'),
    'withTime' => false
])

@formField('date_picker', [
    'name' => 'date_to',
    'label' => trans('experience.date_to'),
    'withTime' => false
])

@formField('medias', [
    'name' => 'logo',
    'label' => trans('experience.logo'),
])
