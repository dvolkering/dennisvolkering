@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => trans('experience.title'),
        'required' => true,
    ])

    @formField('input', [
        'name' => 'role',
        'label' => trans('experience.role'),
    ])

    @formField('wysiwyg', [
        'name' => 'description',
        'label' => trans('experience.description'),
        'editSource' => true
    ])

    @formField('date_picker', [
        'name' => 'date_from',
        'label' => trans('experience.date_from'),
        'withTime' => false
    ])

    @formField('date_picker', [
        'name' => 'date_to',
        'label' => trans('experience.date_to'),
        'withTime' => false
    ])

    @formField('medias', [
        'name' => 'logo',
        'label' => trans('experience.logo'),
    ])

@stop
