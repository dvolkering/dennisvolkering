@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => trans('skills.title'),
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])

@formField('select', [
    'name' => 'type',
    'label' => trans('skills.type'),
    'options' => [
        [
            'value' => 'professional',
            'label' => trans('skills.type_professional'),
        ],
        [
            'value' => 'personal',
            'label' => trans('skills.type_personal'),
        ]
    ]
])

@formField('input', [
    'name' => 'description',
    'label' => trans('skills.description'),
    'maxlength' => 100
])

@formField('input', [
    'name' => 'level',
    'label' => trans('skills.level'),
])
