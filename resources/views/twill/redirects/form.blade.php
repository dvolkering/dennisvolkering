@extends('twill::layouts.form', [
    'additionalFieldsets' => [
        ['fieldset' => 'analytics', 'label' => trans('redirects.analytics')],
    ]
])

@section('contentFields')
    @formField('input', [
        'name' => 'alias',
        'required' => true,
        'prefix' => config('twill.admin_app_url') . '/',
        'maxlength' => 50,
        'label' => trans('redirects.alias')
    ])

    @formField('input', [
        'name' => 'url',
        'required' => true,
        'label' => trans('redirects.original')
    ])

    @formField('input', [
        'name' => 'title',
        'required' => true,
        'label' => trans('redirects.title')
    ])

    @formField('input', [
        'name' => 'description',
        'label' => trans('redirects.description')
    ])
@stop


@section('fieldsets')
    <a17-fieldset id="analytics" title="{{ trans('redirects.analytics') }}" :open="true">
        <canvas id="myChart"></canvas>

        <select id="type">
            <option value="bar" selected>{{ trans('redirects.type.bar') }}</option>
            <option value="line">{{ trans('redirects.type.line') }}</option>
        </select>
    </a17-fieldset>

    @push('extra_js')
        {{-- <script src="{{ mix('js/twill.js') }}" type="text/javascript"></script> --}}

        <script>
            let ctx = document.getElementById('myChart').getContext('2d');
            let dates = {!!  $item->viewsPerDay() !!};

            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    datasets: [{
                        label: 'Clicks',
                        backgroundColor: 'rgba(255, 99, 132, .5)',
                        borderColor: 'rgb(255, 99, 132)',
                        borderWidth: 2,
                        data: dates
                    }]
                },

                // Configuration options go here
                options: {
                    responsive: true,
                    scales: {
                        xAxes: [{
                            type: 'time',
                            offset: true,
                            time: {
                                parser: 'YYYY-MM-DD',
                                unit: 'day',
                                unitStepSize: 1,
                                displayFormats: {
                                    'day': 'YY-MM-DD',
                                }
                            },
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1
                            }
                        }],
                    },
                }
            });


            document.getElementById('type').addEventListener('change', function () {
                var type = document.getElementById('type').value;
                var dataset = chart.config.data.datasets[0];
                dataset.type = type;
                chart.update();
            });
        </script>
    @endpush

@stop

