@formField('input', [
    'name' => 'alias',
    'required' => true,
    'prefix' => config('twill.admin_app_url') . '/',
    'maxlength' => 50,
    'label' => trans('redirects.alias')
])

@formField('input', [
    'name' => 'url',
    'label' => trans('redirects.original')
])

@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => $titleFormKey === 'title' ? twillTrans('twill::lang.modal.title-field') : ucfirst($titleFormKey),
    'translated' => $translateTitle ?? false,
    'required' => true
])

@formField('input', [
    'name' => 'description',
    'label' => trans('redirects.description'),
])
