@formField('input', [
    'name' => 'title',
    'label' => trans('blocks.title'),
])

@formField('input', [
    'name' => 'subtitle',
    'label' => trans('blocks.subtitle'),
])

@formField('wysiwyg', [
    'name' => 'paragraph',
    'label' => trans('blocks.text'),
    'editSource' => true
])
