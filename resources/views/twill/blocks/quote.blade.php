@formField('select', [
    'name' => 'quote_align',
    'label' => trans('blocks.quote.align'),
    'options' => [
        [
            'value' => 'text-left',
            'label' => trans('blocks.quote.align_left'),
        ],
        [
            'value' => 'text-right',
            'label' => trans('blocks.quote.align_right'),
        ],
        [
            'value' => 'text-center',
            'label' => trans('blocks.quote.align_center'),
        ]
    ]
])

@formField('input', [
    'name' => 'quote',
    'label' => trans('blocks.quote.quote'),
    'maxlength' => 250,
    'required' => true,
    'type' => 'textarea',
    'rows' => 3
])

@formField('input', [
    'name' => 'quote_author',
    'label' => trans('blocks.quote.author'),
    'maxlength' => 100,
])
