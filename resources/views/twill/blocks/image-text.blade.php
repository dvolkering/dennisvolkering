@formField('select', [
    'name' => 'block_style',
    'label' => trans('blocks.image_text.style'),
    'unpack' => true,
    'options' => [
        [
            'value' => 'flex-md-row flex-column',
            'label' => trans('blocks.image_text.style_text_left_image_right'),
        ],
        [
            'value' => 'flex-md-row-reverse flex-column',
            'label' => trans('blocks.image_text.style_text_right_image_left')
        ],
    ]
])

@formField('medias', [
    'name' => 'image',
    'label' => 'Image',
    'note' => 'Minimum image width 1300px'
])

@formField('input', [
    'name' => 'image_caption_title',
    'label' => 'Image caption - Title',
    'maxlength' => 250,
])


@formField('input', [
    'name' => 'image_caption_subtitle',
    'label' => 'Image caption - Subtitle',
    'maxlength' => 250,
])

@formField('input', [
    'name' => 'title',
    'label' => 'Text - Title',
    'maxlength' => 250,
])

@formField('input', [
    'name' => 'subtitle',
    'label' => 'Text - Subtitle',
    'maxlength' => 250,
])

@formField('wysiwyg', [
    'name' => 'paragraph',
    'label' => 'Paragraph',
    'editSource' => true,
    'note' => 'You can edit the source.',
])

@formField('select', [
    'name' => 'cta_style',
    'label' => 'Call to action: Style',
    'placeholder' => 'Select a color',
    'options' => [
        [
            'value' => 'btn-success',
            'label' => 'Green'
        ],
        [
            'value' => 'btn-outline-success',
            'label' => 'Green outline'
        ],
        [
            'value' => 'btn-primary',
            'label' => 'Primary'
        ],
        [
            'value' => 'btn-outline-primary',
            'label' => 'Primary outline'
        ],
        [
            'value' => 'btn-secondary',
            'label' => 'Secondary'
        ],
        [
            'value' => 'btn-outline-secondary',
            'label' => 'Secondary outline'
        ],
        [
            'value' => 'btn-link',
            'label' => 'Link'
        ],
    ]
])

@formField('input', [
    'name' => 'cta_link',
    'label' => 'Call to action: Link',
    'note' => 'Link, with https://'
])

@formField('input', [
    'name' => 'cta_text',
    'label' => 'Call to action: Text',
    'maxlength' => 100,
])
