@formField('input', [
    'name' => 'paragraph_title',
    'label' => trans('blocks.title'),
])
@formField('input', [
    'name' => 'paragraph_subtitle',
    'label' => trans('blocks.subtitle'),
])

@formField('wysiwyg', [
    'name' => 'paragraph_text',
    'label' => trans('blocks.text'),
    'editSource' => true
])

@formField('input', [
    'name' => 'contact_title',
    'label' => trans('blocks.contact.title'),
])

@formField('input', [
    'name' => 'contact_subtitle',
    'label' => trans('blocks.contact.subtitle'),
])
