@formField('multi_select', [
    'name' => 'block_style',
    'label' => trans('blocks.banner.style.style'),
    'options' => [
        [
            'value' => 'banner--skewed',
            'label' => trans('blocks.banner.style.skewed'),
        ],
        [
            'value' => 'banner--parallax',
            'label' => trans('blocks.banner.style.parallax'),
        ]
    ]
])

@formField('select', [
    'name' => 'block_size',
    'label' => trans('blocks.banner.size.size'),
    'unpack' => true,
    'options' => [
        [
            'value' => 'banner--small',
            'label' => trans('blocks.banner.size.small'),
        ],
        [
            'value' => 'banner--medium',
            'label' => trans('blocks.banner.size.medium'),
        ],
        [
            'value' => 'banner--large',
            'label' => trans('blocks.banner.size.large'),
        ],
        [
            'value' => 'banner--full-height',
            'label' => trans('blocks.banner.size.fullheight'),
        ],
    ]
])

@formField('medias', [
    'name' => 'image',
    'label' => trans('blocks.banner.image.image')
])

@formField('select', [
    'name' => 'image_position_x',
    'label' => trans('blocks.banner.image.align'),
    'options' => [
        [
            'value' => '0',
            'label' => trans('blocks.banner.image.align_left'),
        ],
        [
            'value' => '50',
            'label' => trans('blocks.banner.image.align_center'),
        ],
        [
            'value' => '100',
            'label' => trans('blocks.banner.image.align_right'),
        ],
    ]
])

@formField('select', [
    'name' => 'title_style',
    'label' => trans('blocks.banner.title.style'),
    'options' => [
        [
            'value' => 'banner--titles-bar',
            'label' => trans('blocks.banner.title.style_bar'),
        ],
        [
            'value' => 'banner--titles-fullheight',
            'label' => trans('blocks.banner.title.style_fullheight'),
        ],
    ]
])

@formField('input', [
    'name' => 'image_title',
    'label' => trans('blocks.title'),
    'maxlength' => 250,
])

@formField('input', [
    'name' => 'image_subtitle',
    'label' => trans('blocks.subtitle'),
    'maxlength' => 250,
])

@formField('select', [
    'name' => 'cta_style',
    'label' => trans('blocks.banner.cta.style'),
    'placeholder' => 'Select a color',
    'options' => [
        [
            'value' => 'btn-success',
            'label' => trans('blocks.banner.cta.style_green'),
        ],
        [
            'value' => 'btn-outline-success',
            'label' => trans('blocks.banner.cta.style_green_outline'),
        ],
        [
            'value' => 'btn-primary',
            'label' => trans('blocks.banner.cta.style_primary'),
        ],
        [
            'value' => 'btn-outline-primary',
            'label' => trans('blocks.banner.cta.style_primary_outline'),
        ],
        [
            'value' => 'btn-secondary',
            'label' => trans('blocks.banner.cta.style_secondary'),
        ],
        [
            'value' => 'btn-outline-secondary',
            'label' => trans('blocks.banner.cta.style_secondary_outline'),
        ],
        [
            'value' => 'btn-link',
            'label' => trans('blocks.banner.cta.style_link'),
        ],
    ]
])

@formField('input', [
    'name' => 'cta_link',
    'label' => trans('blocks.banner.cta.link'),
])

@formField('input', [
    'name' => 'cta_text',
    'label' => trans('blocks.banner.cta.text'),
    'maxlength' => 100,
])
