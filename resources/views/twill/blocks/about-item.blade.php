@formField('select', [
    'name' => 'type',
    'label' => trans('blocks.about.type.type'),
    'options' => [
        [
            'value' => 'regular',
            'label' => trans('blocks.about.type.regular'),
        ],
        [
            'value' => 'skills',
            'label' => trans('blocks.about.type.skills'),
        ],
        [
            'value' => 'experiences',
            'label' => trans('blocks.about.type.experiences'),
        ],
        [
            'value' => 'educations',
            'label' => trans('blocks.about.type.educations'),
        ],
    ]
])

@formField('input', [
    'name' => 'title',
    'label' => trans('blocks.title'),
])

@formField('input', [
    'name' => 'subtitle',
    'label' => trans('blocks.subtitle'),
])

@formField('wysiwyg', [
    'name' => 'description',
    'label' => trans('blocks.text'),
    'editSource' => true
])
