@formField('input', [
    'name' => 'title',
    'label' => trans('blocks.title'),
])

@formField('input', [
    'name' => 'subtitle',
    'label' => trans('blocks.subtitle'),
])

@formField('wysiwyg', [
    'name' => 'description',
    'label' => trans('blocks.title'),
    'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        "clean",
    ],
])
