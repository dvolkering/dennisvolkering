@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'description',
        'label' => trans('article.description'),
        'translated' => true,
        'maxlength' => 100
    ])

    @formField('medias',[
        'name' => 'cover',
        'label' => trans('article.cover'),
    ])

    @formField('block_editor')
@stop
