@extends('twill::layouts.form', [
    'additionalFieldsets' => [
        ['fieldset' => 'seo', 'label' => trans('page.seo')],
        ['fieldset' => 'page_content', 'label' => trans('page.page_content')],
    ]
])

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => trans('page.title'),
    ])
@stop

@section('fieldsets')
    <a17-fieldset id="seo" title="{{ trans('page.seo') }}" :open="true">
        @formField('input', [
            'name' => 'title_seo',
            'label' => trans('page.title'),
            'maxlength' => 80
        ])

        @formField('input', [
            'name' => 'description',
            'label' => trans('page.description'),
            'type' => 'textarea',
            'maxlength' => 150
        ])

        @formField('medias',[
            'name' => 'cover',
            'label' => trans('page.image'),
        ])
    </a17-fieldset>

    <a17-fieldset id="page_content" title="{{ trans('page.page_content') }}" :open="true">
        @formField('block_editor')
    </a17-fieldset>
@stop

