@extends('twill::layouts.form', [
    'additionalFieldsets' => [
        ['fieldset' => 'album_details', 'label' => trans('album.album_details')],
    ]
])

@section('contentFields')
    @formField('input', [
        'name' => 'title_seo',
        'label' => trans('album.title_seo'),
        'maxlength' => 80
    ])

    @formField('input', [
        'name' => 'description',
        'label' => trans('album.description'),
        'type' => 'textarea',
        'maxlength' => 150
    ])

    @formField('medias',[
        'name' => 'cover',
        'label' => trans('album.cover'),
    ])
@stop

@section('fieldsets')
    <a17-fieldset id="album_details" title="{{ trans('album.album_details') }}" :open="true">
        @formField('input', [
            'name' => 'client',
            'label' => trans('album.client'),
            'maxlength' => 255
        ])

        @formField('input', [
            'name' => 'technology',
            'label' => trans('album.technology'),
            'note' => trans('album.technology_note'),
            'maxlength' => 255
        ])

        @formField('wysiwyg', [
            'name' => 'description_extensive',
            'label' => trans('album.description_extensive'),
            'toolbarOptions' => [
                ['header' => [2, 3, 4, 5, 6, false]],
                'bold',
                'italic',
                'underline',
                'strike',
                ["script" => "super"],
                ["script" => "sub"],
                "blockquote",
                "code-block",
                ['list' => 'ordered'],
                ['list' => 'bullet'],
                ['indent' => '-1'],
                ['indent' => '+1'],
                ["align" => []],
                ["direction" => "rtl"],
                'link',
                "clean",
            ],
            'editSource' => true
        ])

        @formField('medias',[
            'name' => 'photos',
            'label' => trans('album.photos'),
            'note' => trans('album.photos_note'),
            'max' => 50
        ])
    </a17-fieldset>
@stop

