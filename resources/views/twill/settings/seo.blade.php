@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => trans('settings.seo.meta_name'),
        'note' => trans('settings.seo.note_meta_name'),
        'name' => 'meta_name',
    ])
    @formField('input', [
        'label' => trans('settings.seo.site_name'),
        'note' => trans('settings.seo.note_site_name'),
        'name' => 'site_name',
        'textLimit' => '80'
    ])
    @formField('input', [
        'label' => trans('settings.seo.default_meta_title'),
        'name' => 'default_meta_title',
        'note' => trans('settings.seo.note_default_meta_title'),
        'maxlength' => 50
    ])
    @formField('input', [
        'label' => trans('settings.seo.separator'),
        'name' => 'meta_title_separator',
        'note' => trans('settings.seo.note_default_meta_separator'),
        'maxlength' => 3
    ])
    @formField('input', [
        'label' => trans('settings.seo.meta_title_prefix'),
        'name' => 'meta_title_prefix',
        'note' => trans('settings.seo.note_title_prefix'),
        'maxlength' => 80
    ])
    @formField('input', [
        'label' => trans('settings.seo.meta_title_prefix_album'),
        'name' => 'meta_title_prefix_album',
        'prefix' => (app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_prefix') . ' ' . app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_separator') . ' '),
        'note' => trans('settings.seo.note_meta_title_prefix_album'),
        'maxlength' => 30
    ])
    @formField('input', [
        'label' => trans('settings.seo.default_meta_description'),
        'name' => 'meta_description',
        'maxlength' => '150',
        'note' => trans('settings.seo.note_default_meta_description'),
        'type' => 'textarea',
    ])
@stop
