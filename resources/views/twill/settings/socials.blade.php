@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => trans('settings.social.url_github'),
        'name' => 'social_github',
        'textLimit' => '250'
    ])
    @formField('input', [
        'label' => trans('settings.social.url_gitlab'),
        'name' => 'social_gitlab',
        'textLimit' => '250'
    ])
    @formField('input', [
        'label' => trans('settings.social.url_instagram'),
        'name' => 'social_instagram',
        'textLimit' => '250'
    ])
    @formField('input', [
        'label' => trans('settings.social.url_linkedin'),
        'name' => 'social_linkedin',
        'textLimit' => '250'
    ])
    @formField('input', [
        'label' => trans('settings.social.url_photography'),
        'name' => 'social_photography',
        'textLimit' => '250'
    ])
@stop
