@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => trans('settings.404.meta_title'),
        'name' => '404_meta_title',
        'maxlength' => 50
    ])
    @formField('input', [
        'label' => trans('settings.404.title'),
        'name' => '404_title',
        'textLimit' => '80'
    ])
    @formField('input', [
        'label' => trans('settings.404.subtitle'),
        'name' => '404_subtitle',
        'textLimit' => '80'
    ])
    @formField('wysiwyg', [
        'label' => trans('settings.404.text'),
        'name' => '404_description',
        'editSource' => true
    ])
    @formField('input', [
        'label' => trans('settings.404.link_text'),
        'name' => '404_button_text',
        'textLimit' => '80'
    ])
@stop
