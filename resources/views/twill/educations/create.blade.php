@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => trans('education.title'),
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])

@formField('input', [
    'name' => 'school',
    'label' => trans('education.school'),
])

@formField('wysiwyg', [
    'name' => 'description',
    'label' => trans('education.description'),
    'editSource' => true
])

@formField('date_picker', [
    'name' => 'date_from',
    'label' => trans('education.date_from'),
    'withTime' => false
])

@formField('date_picker', [
    'name' => 'date_to',
    'label' => trans('education.date_to'),
    'withTime' => false
])
