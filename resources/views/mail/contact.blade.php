@component('mail::message')
    **{{ trans('mail.sender_name') }}:** {{$email->name}}<br/>
    **{{ trans('mail.sender_mail') }}:** {{$email->mail}}<br/>
    **{{ trans('mail.message') }}:**<br/>
    {{ $email->message }}
@endcomponent
