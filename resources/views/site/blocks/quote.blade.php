<section class="row quote">
    <blockquote class="blockquote {{ $block->input('quote_align') }}">
        @if($block->input('quote'))
            <p class="mb-0">
                {{ $block->input('quote') }}
            </p>
        @endif
        @if($block->input('quote_author'))
            <footer class="blockquote-footer">
                {{ $block->input('quote_author') }}
            </footer>
        @endif
    </blockquote>
</section>
