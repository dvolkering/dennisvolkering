<section class="row paragraph">
    <div class="col-md-12">
        <div class="paragraph__content">
            @if($block->input('title'))
                <h2 class="paragraph__title title">
                    {{ $block->input('title') }}
                </h2>
            @endif

            @if($block->input('subtitle'))
                <h5 class="paragraph__subtitle subtitle">
                    {{ $block->input('subtitle') }}
                </h5>
            @endif

            @if($block->input('paragraph'))
                <div class="paragraph__text text">
                    {!! $block->input('paragraph') !!}
                </div>
            @endif
        </div>
    </div>
</section>
