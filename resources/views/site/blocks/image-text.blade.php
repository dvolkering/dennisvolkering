<section class="row image-text">
    <div class="col-12 p-0 align-items-md-center d-flex {{ $block->input('block_style') }}">
        <div class="col-md-4 vh-md-150 position-relative">
            <div class="image-text__content vh-md-100">
                @if($block->input('title'))
                    <h2 class="image-text__title title">
                        {{ $block->input('title') }}
                    </h2>
                @endif

                @if($block->input('subtitle'))
                    <h5 class="image-text__subtitle subtitle">
                        {{ $block->input('subtitle') }}
                    </h5>
                @endif

                @if($block->input('paragraph'))
                    <div class="image-text__text text">
                        {!! $block->input('paragraph') !!}

                        @if($block->input('cta_text'))
                            <a href="{{ $block->input('cta_link') }}"
                               target="_blank"
                               class="btn {{ $block->input('cta_style') }}">
                                {{ $block->input('cta_text') }}
                            </a>
                        @endif
                    </div>

                @endif

            </div>
            @if($block->input('image_caption_title') || $block->input('image_caption_subtitle'))
                <div class="image-text__caption">
                    @if($block->input('image_caption_title'))
                        <h2 class="image-text__title title">
                            {{ $block->input('image_caption_title') }}
                        </h2>
                    @endif
                    @if($block->input('image_caption_subtitle'))
                        <h5 class="image-text__subtitle subtitle">
                            {{ $block->input('image_caption_subtitle') }}
                        </h5>
                    @endif
                </div>
            @endif
        </div>

        <div class="col-md-8 p-0 m-0 position-md-sticky">
            <div class="image-text__img vh-50 vh-md-100">
                <picture>
                    {{-- Large size (original) --}}
                    <source type="image/webp" media="(min-width: 1921px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp']) }}">
                    <source type="image/jpeg" media="(min-width: 1921px)"
                            srcset="{{ $block->image('image', 'default') }}">

                    {{-- Large size (1920px) --}}
                    <source type="image/webp" media="(min-width: 801px) and (max-width: 1920px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp', 'w' => 2000]) }}">
                    <source type="image/jpeg" media="(min-width: 801px) and (max-width: 1920px)"
                            srcset="{{ $block->image('image', 'default', ['w' => 2000]) }}">

                    {{-- Large size (800px) --}}
                    <source type="image/webp" media="(max-width: 800px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp', 'w' => 800]) }}">
                    <source type="image/jpeg" media="(max-width: 800px)"
                            srcset="{{ $block->image('image', 'default', ['w' => 800]) }}">

                    <img class="image-text__img__src vh-50 vh-md-100"
                         src="{{ $block->image('image') }}"
                         alt="{{ $block->imageAltText('image') }}"
                         title="{{ $block->imageCaption('image') }}"
                         loading="lazy">
                </picture>
            </div>
        </div>
    </div>
</section>
