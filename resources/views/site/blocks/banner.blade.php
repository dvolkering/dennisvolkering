<section
    class="row banner {{ $block->input('block_size') }} {{ $block->input('title_style') }} {{ $block->input('block_style') ? implode(' ', $block->input('block_style')) : '' }}">
    <div class="col-md-12 m-0 p-0">
        <div class="banner__content d-flex flex-column">

            <div class="banner__content__image">

                <picture>
                    {{-- Large size (original) --}}
                    <source type="image/webp" media="(min-width: 1921px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp']) }}">
                    <source type="image/jpeg" media="(min-width: 1921px)"
                            srcset="{{ $block->image('image', 'default') }}">

                    {{-- Large size (1920px) --}}
                    <source type="image/webp" media="(min-width: 801px) and (max-width: 1920px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp', 'w' => 2000]) }}">
                    <source type="image/jpeg" media="(min-width: 801px) and (max-width: 1920px)"
                            srcset="{{ $block->image('image', 'default', ['w' => 2000]) }}">

                    {{-- Small size (800px) --}}
                    <source type="image/webp" media="(max-width: 800px)"
                            srcset="{{ $block->image('image', 'default', ['fm' => 'webp', 'w' => 800]) }}">
                    <source type="image/jpeg" media="(max-width: 800px)"
                            srcset="{{ $block->image('image', 'default', ['w' => 800]) }}">

                    <img class="banner__content__image__src"
                         data-objectposition-x="{{ $block->input('image_position_x') }}"
                         src="{{ $block->image('image') }}"
                         alt="{{ $block->imageAltText('image') }}"
                         title="{{ $block->imageCaption('image') }}"
                         style="object-position: {{ $block->input('image_position_x') }}% 50%;"
                         loading="lazy">
                </picture>
            </div>

            <div class="banner__content__text text-center d-flex flex-fill flex-column justify-content-around ">
                @if($block->input('image_title') || $block->input('image_subtitle'))
                    <div class="banner__titles">
                        @if($block->input('image_title'))
                            <h1 class="banner__title ">{{ $block->input('image_title') }}</h1>
                        @endif

                        @if($block->input('image_subtitle'))
                            <hr class="banner__spacer">
                            <h3 class="banner__subtitle ">{{ $block->input('image_subtitle') }}</h3>
                        @endif
                    </div>
                @endif

                @if($block->input('cta_text'))
                    <div class="banner__cta">
                        <a href="{{ $block->input('cta_link') }}"
                           class="btn btn-xl {{ $block->input('cta_style') }}">{{ $block->input('cta_text') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>

</section>
