@inject('skill', 'App\Models\Skill')

<div class="skill__container" id="skill_container">
    @foreach($skill::all()->sortBy('position') as $skill)
        @if($skill->level)
            <div class="skill__item">
                <span class="skill__text">{{ $skill->title }}</span>
                <div class="skill__level">
                    <div class="skill__level__content" data-level="{{ $skill->level }}" style="position: relative">
                        {{ $skill->title }}
                        @include('site.icons.about.icon-rocket')
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>
