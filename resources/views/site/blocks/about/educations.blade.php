@inject('education', 'App\Models\Education')

<div class="education__container">
    @foreach($education::all()->sortBy('position') as $education)
        @if(!$loop->first)
            <hr class="about__spacer">
        @endif
        <div class="education">
            <div class="education__info">
                <h4 class="title about__title about__title--small">{{$education->title}}</h4>
                <h5 class="subtitle about__subtitle about__subtitle--small">{{$education->school}}</h5>
                <span>
                    {{!$education->date_from ?: $education->date_from->format('Y')}} -
                    {{!$education->date_to ? trans('blocks.about.current') : $education->date_to->format('Y')}}
                </span>

                <div class="education__description">
                    {!! $education->description !!}
                </div>
            </div>
        </div>
    @endforeach
</div>
