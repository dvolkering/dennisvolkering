@inject('experience', 'App\Models\Experience')

<div class="experience__container">
    @foreach($experience::all()->sortBy('position') as $experience)
        @if(!$loop->first)
            <hr class="about__spacer">
        @endif
        <div class="experience">
            <div class="experience__logo">
                <img class="experience__logo__src"
                     src="{{ $experience->image('logo') }}"
                     alt="{{$block->imageAltText('image')}}"
                     title="{{$block->imageCaption('image')}}"/>
            </div>
            <div class="experience__info">
                <h4 class="title about__title about__title--small">{{$experience->title}}</h4>
                <h5 class="subtitle about__subtitle about__subtitle--small">{{$experience->role}}</h5>
                <span class="experience__duration">
                    {{!$experience->date_from ?: $experience->date_from->format('m-Y')}} -
                    {{!$experience->date_to ? trans('blocks.about.current') : $experience->date_to->format('m-Y')}}
                    ({{$experience->differenceText()}})
                </span>

                <div class="experience__description">
                    {!! $experience->description !!}
                </div>
            </div>
        </div>
    @endforeach
</div>
