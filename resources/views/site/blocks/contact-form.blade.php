<section class="row contact-form">
    <div class="col-md-5 offset-md-1">
        <h2 class="contact-form__title title">
            {{ $block->input('paragraph_title') }}
        </h2>
        <h1 class="contact-form__subtitle subtitle">
            {{ $block->input('paragraph_subtitle') }}
        </h1>
        <div class="contact-form__text">
            {!! $block->input('paragraph_text') !!}
        </div>
    </div>
    <div class="col-md-5">
        @if($block->input('contact_title'))
            <h2 class="title">
                {{ $block->input('contact_title') }}
            </h2>
            <h5 class="subtitle">
                {{ $block->input('contact_subtitle') }}
            </h5>
        @endif

        <form class="contact-form__form" action="{{url('/send-contact-form')}}" method="post">
            {{ csrf_field() }}

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul class="list-unstyled mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success">
                    <ul class="list-unstyled mb-0">
                        <li>{{ session('success') }}</li>
                    </ul>
                </div>
            @endif

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name">Naam</label>
                    <input class="form-control" id="name" name="name" type="text">
                </div>
                <div class="form-group col-md-6">
                    <label for="mail">E-mail</label>
                    <input class="form-control" id="mail" name="mail" type="email">
                </div>
            </div>
            <div class="form-group">
                <label for="message">Bericht</label>
                <textarea class="form-control" name="message" id="message" cols="30" rows="10"></textarea>
            </div>
            <input type="hidden" name="recaptcha_response" id="recaptcha_response">
            <button type="submit" class="btn btn-outline-primary">Bericht versturen</button>
        </form>
        <script src="https://www.google.com/recaptcha/api.js?render={{ config('recaptcha.key.site') }}"></script>
        <script>
            grecaptcha.ready(function () {
                grecaptcha.execute('{{ config('recaptcha.key.site') }}').then(function (token) {
                    var recaptchaResponse = document.getElementById('recaptcha_response');
                    recaptchaResponse.value = token;
                });
            });
        </script>
    </div>
</section>
