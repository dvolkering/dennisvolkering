@inject('albums', 'App\Models\Album')

<div class="row mt-3">
    <div class="col-sm-12 col-md-4 col-lg-3 position-sticky-md">
        <h1 class="title">{{ $block->input('title') }}</h1>
        <h2 class="subtitle">{{ $block->input('subtitle') }}</h2>
        <hr class="bg-secondary">
        {!! $block->input('description') !!}
    </div>
    <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="albums">
            @foreach($albums::all()->where('published', '=', '1')->sortBy('position') as $album)
                <div class="albums__album">
                    <a href="{{ trans('album.permalink') . '/' . $album->slug }}" class="albums__album__link">
                        <picture>
                            <source type="image/webp" srcset="{{ $album->image('cover', 'flexible', ['w' => 700, 'fm' => 'webp']) }}">
                            <img class="img-responsive albums__album__image"
                                 src="{{ $album->image('cover', 'flexible', ['w' => 700]) }}"
                                 alt="{{ $album->title }}"
                                 title="{{ $album->title }}"
                                 loading="lazy">
                        </picture>
                        <h3 class="albums__album__title">{{ $album->title }}</h3>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
