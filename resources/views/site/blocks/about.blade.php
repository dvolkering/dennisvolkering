<div class="row">
    <div class="col-sm-12 p-0">
        <div class="about" id="about">
            <div class="about__background" id="about_background"></div>
            <div class="about__icons" id="about_icons">
                @foreach([ 'sun', 'mercury', 'venus', 'earth', 'moon', 'iss', 'camera', 'mars', 'jupiter','cactus', 'saturn', 'uranus', 'neptune'] as $icon)
                    <div class="icon icon--{{ $icon }}">
                        @include('site.icons.about.icon-' . $icon)
                    </div>
                @endforeach
            </div>
            <div class="about__content">
                <h1 class="text-center title mt-5">{{ trans('blocks.about.about_me') }}</h1>
                @if($block->children)
                    @foreach($block->children as $child)
                        <section class="about__content__item {{ $loop->even ? 'about__content__item--even' : 'about__content__item--odd' }}">
                            <h2 class="title">{{ $child->input('title')}}</h2>
                            <h3 class="subtitle">{{ $child->input('subtitle')}}</h3>
                            <div class="text">{!! $child->input('description') !!}</div>

                            @includeIf('site.blocks.about.' . $child->input('type'), ['block' => $child])
                        </section>
                    @endforeach
                @endif
            </div>
        </div>

        <script type="text/javascript">
            window.addEventListener('scroll', onScroll);

            function onScroll() {
                parallaxAbout();
                setSkillWidth();
            }

            /**
             * Set all parallax effects to the about page
             */
            function parallaxAbout() {
                let about = document.getElementById('about');
                let aboutBg = document.getElementById('about_background');
                let icons = document.getElementById('about_icons');
                if (about) {
                    aboutBg.style.backgroundPositionY = (window.pageYOffset) / 1.2 + 'px'
                    icons.style.top = -(window.pageYOffset) / 3 + 'px'
                }
            }

            /**
             * Add animations to the skill bars
             */
            function setSkillWidth() {
                const skills = document.getElementsByClassName('skill__level__content');
                const skillContainerWidth = document.getElementById('skill_container').offsetWidth;

                Array.prototype.forEach.call(skills, function (skill) {
                    const offset = skill.offsetTop - (window.innerHeight / 1.3);

                    if (offset < window.pageYOffset) {
                        // Set the bar width
                        skill.style.width = skill.dataset.level + '%';
                        skill.classList.add('skill__level__content--active');

                        // Shoot the rocket to the right
                        skill.getElementsByClassName('rocket')[0].style.right = skillContainerWidth * -1;
                    }
                });
            }
        </script>
    </div>
</div>
