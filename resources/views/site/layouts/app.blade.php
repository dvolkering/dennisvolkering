<!DOCTYPE html>
<html dir="ltr" lang="nl-NL">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Favicons --}}
    <link rel="icon" href="/images/favicon/32.png" sizes="32x32">
    <link rel="icon" href="/images/favicon/192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="/images/favicon/180.png">
    <meta name="msapplication-TileImage" content="/images/favicon/270.png">

    {{-- Basic meta --}}
    <title>
        {{$title = $meta_title ?? $item->getFullSeoTitle($item->title_seo) ??
        app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_prefix') . ' ' .
        app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_separator') . ' ' .
        app('A17\Twill\Repositories\SettingRepository')->byKey('default_meta_title') }}
    </title>
    <meta name="description" content="{{ $item->description ?? '' }}">

    <link rel="canonical" href="{{ $meta_url = Request::url() ?? '' }}">
    <link rel="shortlink" href="{{ $meta_url ?? '' }}">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">

    {{-- Meta color --}}
    <meta name="msapplication-TileColor" content="{{ $meta_color = app('A17\Twill\Repositories\SettingRepository')->byKey('site_color') ?? '' }}">
    <meta name="theme-color" content="{{ $meta_color ?? '' }}">

    {{-- OpenGraph tags --}}
    <meta property="og:locale" content="nl_NL">
    <meta property="og:site_name" content="{{ app('A17\Twill\Repositories\SettingRepository')->byKey('site_name') ?? '' }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $title }}">
    <meta property="og:description" content="{{ $item->description ?? '' }}">
    <meta property="og:url" content="{{ $meta_url ?? '' }}">

    @isset($item)
        <meta property="og:image" content="{{ $img = $item->imageObject('cover', 'flexible') ? $item->image('cover', 'flexible') : '' }}">
        <meta property="og:image:secure_url" content="{{ $img }}">
        <meta property="og:image:width" content="{{ $item->imageObject('cover', 'flexible')->width ?? '' }}">
        <meta property="og:image:height" content="{{ $item->imageObject('cover', 'flexible')->height ?? '' }}">
    @endisset

    {{-- Twitter tags --}}
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="{{ $item->description ?? '' }}">
    <meta name="twitter:title" content="{{ $title }}">

    @isset($img)
        <meta name="twitter:image" content="{{ $img }}">
    @endisset

    @isset($meta_schema)
        @foreach($meta_schema as $schema)
            {!! $schema !!}
        @endforeach
    @endisset

    {{-- Styles --}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @if(config('analytics.tracking_id'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('analytics.tracking_id') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', '{{ config('analytics.tracking_id') }}');
        </script>
    @endif
</head>

<body>
<div class="nav-toggle" id="nav_toggle">
    <span class="nav-toggle__text">MENU</span>
    <div class="nav-toggle__burger nav-toggle__burger--first"></div>
    <div class="nav-toggle__burger nav-toggle__burger--second"></div>
    <div class="nav-toggle__burger nav-toggle__burger--third"></div>
</div>

<nav class="menu--ontop menu text-light d-flex justify-content-around" id="navigation">
    @include('site.components.navigation')
</nav>

<div class="site--menu-is-on-top site" id="site">
    <div class="container-fluid site__content border-bottom border-secondary">
        @yield('content')
    </div>
    <div class="footer__spacer"></div>
    <footer class="footer" id="footer">
        @include('site.components.footer')
    </footer>
</div>

{{-- Scripts --}}
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

@yield('scripts')
</body>
</html>
