@extends('site.layouts.app', [
    'meta_schema' => $item->getSchema()
])

@section('content')
    <div class="row mt-3">
        <div class="col-sm-12 col-md-4 col-lg-3 position-sticky-md">
            <h1>{{ $item->title }}</h1>
            {!!  $item->description_extensive !!}
            <hr class="bg-secondary">
            @if($item->client)
                <h4 class="badge badge-primary">Klant: {{ $item->client }}</h4>
            @endif
            @if($item->technology)
                @foreach(explode(",", $item->technology) as $tech)
                    <h4 class="badge badge-success">{{ $tech }}</h4>
                @endforeach
            @endif
        </div>
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="album">
                @foreach($item->getAllImages('photos', 'flexible') as $photo)
                    <a href="{{$photo['full']}}" class="album__photo" title="{{ $photo['data']['caption'] }}">
                        <div class="album__photo__img">
                            <picture class="img-responsive album__photo__src">
                                <source type="image/webp" srcset="{{$photo['thumb']['webp']}}">
                                <img class="img-responsive album__photo__src" src="{{$photo['thumb']['original']}}" alt="{{ $photo['data']['alt'] }}" loading="lazy">
                            </picture>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>




    <div class='hero'>
        @if( $item->hasImage('cover'))
            <img src="{{ $item->image('cover', 'flexible') }}">
        @endif


    </div>

    <link href="{{ mix('css/lightbox.css') }}" rel="stylesheet"/>
    <script src="{{ mix('js/lightbox.js') }}" type="text/javascript"></script>

    <script>
        // or just using browser globals
        let SimpleLightbox = window.SimpleLightbox;

        // intance via constructor and selector
        var lightbox = new SimpleLightbox({elements: '.album a'});
    </script>
@endsection



