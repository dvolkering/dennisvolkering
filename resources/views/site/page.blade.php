@extends('site.layouts.app', [
    'meta_schema' => $item->getSchema()
])

@section('content')
    {!! $item->renderBlocks() !!}
@endsection
