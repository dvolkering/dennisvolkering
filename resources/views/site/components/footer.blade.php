<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h2 class="footer__title title">{{ app('A17\Twill\Repositories\SettingRepository')->byKey('footer_title') }}</h2>
            <h4 class="footer__subtitle subtitle">{{ app('A17\Twill\Repositories\SettingRepository')->byKey('footer_subtitle') }}</h4>
            <p class="footer__text text">
                {{ app('A17\Twill\Repositories\SettingRepository')->byKey('footer_description') }}
            </p>
            <div class="footer__icons">
                @include('site.icons.icon-github')
                @include('site.icons.icon-gitlab')
                @include('site.icons.icon-instagram')
                @include('site.icons.icon-linkedin')
                @include('site.icons.icon-photography')
            </div>
        </div>
        <div class="col-md-6">
            <ul class="menu__list">
                <li class="menu__list__item">
                    <a class="menu__list__item__link" href="/">Home</a>
                </li>

                @isset($menu)
                    @foreach($menu as $item)
                        <li class="menu__list__item">
                            <a class="menu__list__item__link" href="/{{$item->slug}}">{{$item->title}}</a>
                        </li>
                    @endforeach
                @endisset
            </ul>
        </div>
    </div>
</div>






