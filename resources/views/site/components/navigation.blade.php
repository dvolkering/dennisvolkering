<ul class="menu__list">
    <li class="menu__list__item menu__list__item--active">
        <a class="menu__list__item__link" href="/" title="Homepagina">Home</a>
    </li>

    @isset($menu)
        @foreach($menu as $item)
            <li class="menu__list__item">
                <a class="menu__list__item__link" href="/{{$item->slug}}">{{$item->title}}</a>
            </li>
        @endforeach
    @endisset
</ul>

<div class="footer__icons">
    @include('site.icons.icon-github')
    @include('site.icons.icon-gitlab')
    @include('site.icons.icon-instagram')
    @include('site.icons.icon-linkedin')
    @include('site.icons.icon-photography')
</div>
