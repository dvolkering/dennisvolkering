<!DOCTYPE html>
<html dir="ltr" lang="nl-NL">

<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title></title>
</head>
<body>
@yield('content')
</body>
</html>
