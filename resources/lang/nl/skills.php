<?php

return [
    'title'             => 'Titel',
    'type'              => 'Type',
    'type_professional' => 'Professioneel',
    'type_personal'     => 'Persoonlijk',
    'description'       => 'Beschrijving',
    'level'             => 'Niveau',
    'date_from'         => 'Datum (Begin)',
    'date_to'           => 'Datum (Eind)',
];
