<?php

return [
    'settings' => 'Instellingen',
    '404'      => [
        '404'        => '404',
        'title'      => 'Titel',
        'meta_title' => 'Meta titel',
        'subtitle'   => 'Subtitel',
        'text'       => 'Tekst',
        'link_text'  => 'Link tekst',
    ],
    'social'   => [
        'socials'         => 'Socials',
        'url_github'      => 'URL Github',
        'url_gitlab'      => 'URL Gitlab',
        'url_instagram'   => 'URL Instagram',
        'url_linkedin'    => 'URL Linkedin',
        'url_photography' => 'URL Fotografie Portfolio',
    ],
    'seo'      => [
        'seo'                           => 'SEO',
        'name'                          => 'Naam',
        'meta_name'                     => 'Meta naam (structured data)',
        'site_name'                     => 'Site naam',
        'default_meta_title'            => 'Standaard metatitel',
        'seperator'                     => 'Metatitel seperator',
        'meta_title_prefix'             => 'Metatitel prefix',
        'meta_title_prefix_album'       => 'Metatitel prefix (Album)',
        'default_meta_description'      => 'Standaard metabeschrijving',
        'default_meta_description_note' => 'Maximaal 150 karakters',
        'footer'                        => [
            'title'            => 'Footer titel',
            'subtitle'         => 'Footer subtitel',
            'description'      => 'Footer beschrijving',
            'description_note' => 'Maximaal 180 karakters',
        ]
    ],
];
