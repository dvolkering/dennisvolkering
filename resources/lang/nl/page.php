<?php

return [
    'seo'          => 'SEO',
    'page_content' => 'Pagina inhoud',
    'title'        => 'Titel',
    'description'  => 'Metabeschrijving',
    'image'        => 'Afbeelding',
];
