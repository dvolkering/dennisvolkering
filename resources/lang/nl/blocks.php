<?php

return [
    'title'       => 'Titel',
    'subtitle'    => 'Subtitel',
    'text'        => 'Tekst',
    'description' => 'Beschrijving',
    'banner'      => [
        'style' => [
            'style'    => 'Stijl',
            'skewed'   => 'Scheef',
            'parallax' => 'Parallax',
        ],
        'size'  => [
            'size'       => 'Grootte',
            'small'      => 'Klein',
            'medium'     => 'Medium',
            'large'      => 'Groot',
            'fullheight' => 'Volledige hoogte',
        ],
        'image' => [
            'image'        => 'Afbeelding',
            'align'        => 'Uitlijning afbeelding',
            'align_left'   => 'Links',
            'align_center' => 'Midden',
            'align_right'  => 'Rechts',
        ],
        'title' => [
            'style'            => 'Titelstijl',
            'style_bar'        => 'Bar',
            'style_fullheight' => 'Volledige hoogte',
            'align_right'      => 'Rechts',
        ],
        'cta'   => [
            'style'                   => 'CTA: Stijl',
            'style_green'             => 'Green',
            'style_green_outline'     => 'Green (Outline)',
            'style_primary'           => 'Primary',
            'style_primary_outline'   => 'Primary (Outline)',
            'style_secondary'         => 'Secondary',
            'style_secondary_outline' => 'Secondary (Outline)',
            'style_link'              => 'Link',
            'link'                    => 'CTA: Link',
            'text'                    => 'CTA: Tekst',
        ],
    ],
    'contact'     => [
        'contact_form' => 'Contactformulier',
        'title'        => 'Contact titel',
        'subtitle'     => 'Contact subtitel',
    ],
    'image-text'  => [
        'image-text'                  => 'Afbeelding met tekst',
        'style'                       => 'Stijl',
        'style_text_left_image_right' => 'Tekst links, afbeelding rechts',
        'style_text_right_image_left' => 'Tekst rechts, afbeelding links',
    ],
    'paragraph'   => [
        'paragraph' => 'Paragraaf',
    ],
    'about'       => [
        'about_me' => 'Over mij',
        'current'  => 'heden',
        'year'     => ':year jaar',
        'month'    => '{1} :month maand|[2,Inf]:month maanden',
        'type'     => [
            'type'        => 'Type',
            'regular'     => 'Normaal',
            'skills'      => 'Skills',
            'educations'  => 'Opleidingen',
            'experiences' => 'Ervaring',
        ],
    ],
    'quote'       => [
        'quote'        => 'Quote',
        'align'        => 'Uitlijning',
        'align_left'   => 'Links',
        'align_center' => 'Midden',
        'align_right'  => 'Rechts',
        'author'       => 'Auteur',
    ],
];
