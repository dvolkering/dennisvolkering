<?php

return [
    'permalink' => 'fotografie',
    'title' => 'Titel',
    'title_seo' => 'SEO Titel',
    'description' => 'SEO Beschrijving',
    'cover' => 'Cover',
    'album_details' => 'Details',
    'client' => 'Klant',
    'technology' => 'Technologie',
    'technology_note' => 'Kommagescheiden',
    'description_extensive' => 'Uitgebreide beschrijving',
    'photos' => 'Foto\'s',
    'photos_note' => 'Maximaal 50 foto\'s',
];
