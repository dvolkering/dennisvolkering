<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'alias' => 'Alias',
    'original' => 'Originele URL',
    'title' => 'Titel',
    'description' => 'Beschrijving',
    'analytics' => 'Analytics',
    'type' => [
        'line' => 'Lijndiagram',
        'bar' => 'Staafdiagram',
    ],
];
