<?php

return [
    'title'       => 'Titel',
    'description' => 'Beschrijving',
    'role'        => 'Rol',
    'date_from'   => 'Datum (Begin)',
    'date_to'     => 'Datum (Eind)',
    'logo'        => 'Logo',
];
