<?php

return [
    'pages'    => [
        'title'  => 'Pages',
        'module' => true,
    ],
    'about'    => [
        'title'              => 'About',
        'route'              => 'twill.about.skills.index',
        'primary_navigation' => [
            'skills'      => [
                'title'  => 'Skills',
                'module' => true,
            ],
            'educations'  => [
                'title'  => 'Education',
                'module' => true,
            ],
            'experiences' => [
                'title'  => 'Experience',
                'module' => true,
            ],
        ],
    ],
    'redirects' => [
        'title'  => 'Redirects',
        'module' => true
    ],
    'albums' => [
        'title'  => 'Albums',
        'module' => true
    ],
    'featured' => [
        'title'              => 'Menu',
        'route'              => 'twill.featured.homepage',
        'primary_navigation' => [
            'homepage' => [
                'title' => 'Homepage',
                'route' => 'twill.featured.homepage',
            ],
        ],
    ],
    'settings' => [
        'title'              => 'Settings',
        'route'              => 'twill.settings',
        'params'             => [
            'section' => 'seo'
        ],
        'primary_navigation' => [
            'seo'     => [
                'title'  => 'SEO',
                'route'  => 'twill.settings',
                'params' => [
                    'section' => 'seo',
                ]
            ],
            'socials' => [
                'title'  => 'Socials',
                'route'  => 'twill.settings',
                'params' => [
                    'section' => 'socials',
                ]
            ],
            '404'     => [
                'title'  => '404',
                'route'  => 'twill.settings',
                'params' => [
                    'section' => '404',
                ]
            ],
        ]
    ],
];
