<?php

return [
    'locale'       => 'nl',
    'enabled'      => [
        'buckets'     => true,
        'activitylog' => true,
    ],
    'frontend'     => [
        'views_path' => 'site',
    ],
    'glide'        => [
        'default_params' => [
            'fm' => null,
            'q'  => 80,
        ],
        'original_media_for_extensions' => ['svg']
    ],
    'dashboard'    => [
        'modules'   => [
            'pages'       => [
                'name'           => 'pages',
                'label'          => 'pages',
                'label_singular' => 'page',
                'count'          => true,
                'create'         => true,
                'activity'       => true,
                'draft'          => true,
                'search'         => true,
            ],
            // 'redirects'   => [
            //     'name'           => 'redirects',
            //     'label'          => 'redirects',
            //     'label_singular' => 'redirect',
            //     'count'          => true,
            //     'create'         => true,
            //     'activity'       => true,
            //     'search'         => true,
            // ],
            // 'skills'      => [
            //     'name'        => 'skills',
            //     'routePrefix' => 'about',
            //     'count'       => true,
            //     'create'      => true,
            //     'activity'    => true,
            //     'search'      => true,
            // ],
            // 'educations'  => [
            //     'name'        => 'educations',
            //     'routePrefix' => 'about',
            //     'count'       => true,
            //     'create'      => true,
            //     'activity'    => true,
            //     'search'      => true,
            // ],
            // 'experiences' => [
            //     'name'        => 'experiences',
            //     'routePrefix' => 'about',
            //     'count'       => true,
            //     'create'      => true,
            //     'activity'    => true,
            //     'search'      => true,
            // ],
            // 'albums'      => [
            //     'name'     => 'albums',
            //     'count'    => true,
            //     'create'   => true,
            //     'activity' => true,
            //     'search'   => true,
            // ],
        ],
    ],
    'buckets'      => [
        'homepage' => [
            'name'    => 'Home',
            'buckets' => [
                'homepage' => [
                    'name'        => 'Homepagina',
                    'bucketables' => [
                        [
                            'module' => 'pages',
                            'name'   => 'Pages',
                            'scopes' => ['published' => true],
                        ],
                    ],
                    'max_items'   => 1,
                ],
                'menu'     => [
                    'name'        => 'Menu',
                    'bucketables' => [
                        [
                            'module' => 'pages',
                            'name'   => 'Pages',
                            'scopes' => ['published' => true],
                        ],
                    ],
                    'max_items'   => 10,
                ],
            ],
        ],
    ],
    'block_editor' => [
        'block_preview_render_childs' => true,
        'blocks'                      => [
            'banner'       => [
                'title'     => 'Banner',
                'icon'      => 'image-text',
                'component' => 'a17-block-banner',
            ],
            'quote'        => [
                'title'     => 'Quote',
                'icon'      => 'quote',
                'component' => 'a17-block-quote',
            ],
            'paragraph'    => [
                'title'     => 'Paragraph',
                'icon'      => 'text',
                'component' => 'a17-block-paragraph',
            ],
            'image-text'   => [
                'title'     => 'Image & Text',
                'icon'      => 'image-text',
                'component' => 'a17-block-image-text',
            ],
            'contact-form' => [
                'title'     => 'Contact form',
                'icon'      => 'add',
                'component' => 'a17-block-contact-form',
            ],
            'about'        => [
                'title'     => 'About',
                'icon'      => 'text',
                'component' => 'a17-block-about',
            ],
            'album'        => [
                'title'     => 'Albums',
                'icon'      => 'image',
                'component' => 'a17-block-album',
            ],
        ],
        'repeaters'                   => [
            'about-item' => [
                'title'     => 'About - Item',
                'trigger'   => 'Add about item',
                'component' => 'a17-block-about-item',
                'max'       => 10
            ],
        ],
        'crops'                       => [
            'image' => [
                'default' => [
                    [
                        'name' => 'default'
                    ],
                ],
            ],
            'cover' => [
                'default' => [
                    [
                        'name' => 'default'
                    ],
                ],
            ]
        ],
    ],
    'blocks_table' => 'blocks',
    'features_table' => 'features',
    'settings_table' => 'settings',
    'medias_table' => 'medias',
    'mediables_table' => 'mediables',
    'files_table' => 'files',
    'fileables_table' => 'fileables',
    'related_table' => 'related',
    'tags_table' => 'tags',
    'tagged_table' => 'tagged',
];
