<?php

return [

    /*
     * Recaptcha keys
     */
    'key' => [
        'site'   => env('RECAPTCHA_SITE_KEY'),
        'secret' => env('RECAPTCHA_SECRET_KEY'),
    ]
];
