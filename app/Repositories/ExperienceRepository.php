<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Experience;

class ExperienceRepository extends ModuleRepository
{
    use HandleMedias;

    public function __construct(Experience $model)
    {
        $this->model = $model;
    }
}
