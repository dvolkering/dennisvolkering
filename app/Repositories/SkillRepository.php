<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Skill;

class SkillRepository extends ModuleRepository
{

    public function __construct(Skill $model)
    {
        $this->model = $model;
    }
}
