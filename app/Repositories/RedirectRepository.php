<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Redirect;

class RedirectRepository extends ModuleRepository
{
    public function __construct(Redirect $model)
    {
        $this->model = $model;
    }

    public function prepareFieldsBeforeCreate(array $fields): array
    {
        $fields['user_id'] = auth('twill_users')->user()->getAuthIdentifier();

        return parent::prepareFieldsBeforeCreate($fields);
    }
}
