<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Education;

class EducationRepository extends ModuleRepository
{
    use HandleMedias;

    public function __construct(Education $model)
    {
        $this->model = $model;
    }
}
