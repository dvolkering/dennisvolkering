<?php

namespace App\Observers;

use App\Console\Commands\GenerateSitemap;
use App\Models\Page;

/**
 * Class PageObserver
 * @package App\Observers
 *
 * Generate the sitemap after a page save/delete
 */
class PageObserver
{
    private $generator;

    public function __construct()
    {
        $this->generator = new GenerateSitemap();
    }

    private function generateSitemap(\App\Models\Page $page)
    {
        if ($page->published) {
            $this->generator->handle();
        }
    }

    /**
     * Handle the page "created" event.
     *
     * @param Page $page
     *
     * @return void
     */
    public function created(Page $page)
    {
        $this->generateSitemap($page);
    }

    /**
     * Handle the page "updated" event.
     *
     * @param Page $page
     *
     * @return void
     */
    public function updated(Page $page)
    {
        $this->generateSitemap($page);
    }

    /**
     * Handle the page "deleted" event.
     *
     * @param Page $page
     *
     * @return void
     */
    public function deleted(Page $page)
    {
        $this->generateSitemap($page);
    }

    /**
     * Handle the page "restored" event.
     *
     * @param Page $page
     *
     * @return void
     */
    public function restored(Page $page)
    {
        $this->generateSitemap($page);
    }

    /**
     * Handle the page "force deleted" event.
     *
     * @param Page $page
     *
     * @return void
     */
    public function forceDeleted(Page $page)
    {
        $this->generateSitemap($page);
    }
}
