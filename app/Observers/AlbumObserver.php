<?php

namespace App\Observers;

use App\Console\Commands\GenerateSitemap;
use App\Models\Album;

/**
 * Class AlbumObserver
 * @package App\Observers
 *
 * Generate the sitemap after a Album save/delete
 */
class AlbumObserver
{
    private $generator;

    public function __construct()
    {
        $this->generator = new GenerateSitemap();
    }

    private function generateSitemap(\App\Models\Album $album)
    {
        if ($album->published) {
            $this->generator->handle();
        }
    }

    /**
     * Handle the Album "created" event.
     *
     * @param Album $album
     *
     * @return void
     */
    public function created(Album $album)
    {
        $this->generateSitemap($album);
    }

    /**
     * Handle the Album "updated" event.
     *
     * @param Album $album
     *
     * @return void
     */
    public function updated(Album $album)
    {
        $this->generateSitemap($album);
    }

    /**
     * Handle the Album "deleted" event.
     *
     * @param Album $album
     *
     * @return void
     */
    public function deleted(Album $album)
    {
        $this->generateSitemap($album);
    }

    /**
     * Handle the Album "restored" event.
     *
     * @param Album $album
     *
     * @return void
     */
    public function restored(Album $album)
    {
        $this->generateSitemap($album);
    }

    /**
     * Handle the Album "force deleted" event.
     *
     * @param Album $album
     *
     * @return void
     */
    public function forceDeleted(Album $album)
    {
        $this->generateSitemap($album);
    }
}
