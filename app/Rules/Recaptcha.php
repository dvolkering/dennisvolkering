<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Get recaptcha response
        $recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('recaptcha.key.secret') . '&response=' . $value);
        $recaptcha = json_decode($recaptcha);

        // Check if response is valid
        if ($recaptcha->success === false) {
            return false;
        }

        // Take action based on the score
        if ($recaptcha->score >= 0.5) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Error in recaptcha';
    }
}
