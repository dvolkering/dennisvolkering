<?php

namespace App\Providers;

use App\Models\Album;
use App\Models\Page;
use App\Observers\AlbumObserver;
use App\Observers\PageObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Relation::morphMap([
            'pages' => 'App\Models\Page',
        ]);

        Page::observe(PageObserver::class);
        Album::observe(AlbumObserver::class);
    }
}
