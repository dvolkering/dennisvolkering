<?php

namespace App\Providers;

use A17\Twill\Models\Feature;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            // Add the menu items to all pages
            $menu = Feature::where('bucket_key', 'menu')
                                 ->with('featured')
                                 ->get()
                                 ->sortBy('position')
                                 ->map(function ($item) {
                                     return $item->featured;
                                 })
                                 ->values() ?? null;

            $view->with('menu', $menu);
        });
    }
}
