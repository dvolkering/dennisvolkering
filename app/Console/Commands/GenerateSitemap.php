<?php

namespace App\Console\Commands;

use A17\Twill\Models\Feature;
use App\Models\Album;
use App\Models\Page;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a sitemap.';

    /**
     * Generate the sitemap.
     */
    public function handle()
    {
        // modify this to your own needs
        $sitemap  = Sitemap::create()->add(Url::create(config('app.url')));
        $homeSlug = Feature::where('bucket_key', 'homepage')->with('featured')->first()->featured->slug ?? null;

        // Add pages
        Page::all()->each(function (Page $page) use ($sitemap, $homeSlug) {
            if ($page->slug !== $homeSlug && $page->published) {
                $sitemap->add(Url::create("/{$page->slug}"));
            }
        });

        // Add albums to sitemap
        Album::all()->each(function (Album $album) use ($sitemap) {
            if ($album->published) {
                $sitemap->add(Url::create( '/' . trans('album.permalink') . "/{$album->slug}"));
            }
        });

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}
