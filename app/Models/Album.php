<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\Sortable;
use App\Models\Abstracts\AbstractWebpage;

class Album extends AbstractWebpage implements Sortable
{
    use HasSlug, HasMedias, HasPosition;

    // Copied from images() function
    public function getAllImages($role, $crop = "default") {
        $medias = $this->medias->filter(function ($media) use ($role, $crop) {
            return $media->pivot->role === $role && $media->pivot->crop === $crop;
        });

        $urls = [];

        foreach ($medias as $key => $media) {
            $urls[$key]['data'] = $this->imageAsArray($role, $crop, [], $media);
            $urls[$key]['thumb']['original'] = $this->image($role, $crop, ['w' => 500], false, false, $media);
            $urls[$key]['thumb']['webp'] = $this->image($role, $crop, ['w' => 500, 'fm' => 'webp'], false, false, $media);
            $urls[$key]['full'] = $this->image($role, $crop, [], false, false, $media);
        }

        return $urls;
    }


    /**
     * Get the prefix for a video page
     *
     * @return string The video prefix defined in the Twill settings
     */
    protected function getSeoPrefix()
    {
        return app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_prefix_album');
    }

    protected $fillable = [
        'published',
        'title',
        'title_seo',
        'description',
        'position',
        'excerpt',
        'description_extensive',
        'client',
        'technology',
    ];
    public $slugAttributes = [
        'title',
    ];
    public $mediasParams = [
        'cover'  => [
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0,
                ],
                [
                    'name'  => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name'  => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'photos' => [
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0
                ]
            ]
        ],
    ];
}
