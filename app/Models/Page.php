<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Models\Abstracts\AbstractWebpage;
use Spatie\SchemaOrg\Schema;

class Page extends AbstractWebpage implements Sortable
{
    use HasBlocks, HasSlug, HasMedias, HasRevisions, HasPosition;
    /**
     * Default page does not have a prefix
     *
     * @return null
     */
    protected function getSeoPrefix() {
        return null;
    }

    protected $fillable = [
        'published',
        'title',
        'title_seo',
        'description',
        'position',
    ];
    public $slugAttributes = [
        'title',
    ];
    public $mediasParams = [
        'cover' => [
            'desktop'  => [
                [
                    'name'  => 'desktop',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile'   => [
                [
                    'name'  => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0,
                ],
                [
                    'name'  => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name'  => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

}
