<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class AlbumSlug extends Model
{
    protected $table = "album_slugs";
}
