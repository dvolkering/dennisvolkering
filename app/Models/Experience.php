<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Illuminate\Support\Facades\Lang;

class Experience extends Model implements Sortable
{
    use HasMedias, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'role',
        'date_from',
        'date_to',
        'default',
        'position',
    ];
    protected $dates = [
        'date_from',
        'date_to',
    ];
    public $mediasParams = [
        'logo' => [
            'default' => [
                [
                    'name'  => 'default',
                    'ratio' => 0,
                ],
            ]
        ],
    ];


    /**
     * Calculate the difference between the from- and to date
     */
    public function difference()
    {
        if ($this->date_from && $this->date_to) {
            return $this->date_from->diff($this->date_to);
        } elseif ($this->date_from) {
            return $this->date_from->diff(today());
        }

        return;
    }

    /**
     * Create a human-readable text with the date_from and date_to difference
     *
     * @return string A readable time difference
     */
    public function differenceText()
    {
        $diff = $this->difference();

        $year  = $diff->format('%y') != 0 ?  trans('blocks.about.year', ['year' => $diff->format('%y')]) : '';
        $month = $diff->format('%m') != 0 ? Lang::choice('blocks.about.month', $diff->format('%m'), ['month' => $diff->format('%m')]) : '';
        $between = $year && $month ? ', ' : '';

        return $year . $between .  $month;
    }
}
