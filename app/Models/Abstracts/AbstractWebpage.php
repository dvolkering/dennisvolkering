<?php

namespace App\Models\Abstracts;

use A17\Twill\Models\Model;
use Spatie\SchemaOrg\Schema;

class AbstractWebpage extends Model
{
    protected $seoTitle;

    /**
     * Get the full SEO Title of the page
     *
     * @param $pageTitle string The title of the page itself
     *
     * @return string The full title
     */
    public function getFullSeoTitle($pageTitle) {
        if(!$pageTitle) {
            return null;
        }

        $defaultPrefix = app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_prefix');
        $separator = app('A17\Twill\Repositories\SettingRepository')->byKey('meta_title_separator');
        $sub = $this->getSeoPrefix() ? $this->getSeoPrefix() . ' ' . $separator : '';

        $this->seoTitle = $defaultPrefix . ' ' . $separator . ' ' . $sub . ' ';

        return $this->seoTitle . $pageTitle;
    }

    /**
     * Build schema.org markup
     *
     * @return array Different schema markups
     */
    public function getSchema()
    {
        $schemas = [];
        $image   = null;

        if ($this->imageObject('cover', 'flexible')) {
            $image = Schema::imageObject()
                           ->identifier('#image')
                           ->image($this->image('cover', 'flexible'))
                           ->width($this->imageObject('cover', 'flexible')->width ?? null)
                           ->height($this->imageObject('cover', 'flexible')->height ?? null);
        }

        $person = Schema::person()
                        ->identifier('#person')
                        ->name(app('A17\Twill\Repositories\SettingRepository')->byKey('meta_name'))
                        ->sameAs([
                            app('A17\Twill\Repositories\SettingRepository')->byKey('social_github'),
                            app('A17\Twill\Repositories\SettingRepository')->byKey('social_gitlab'),
                            app('A17\Twill\Repositories\SettingRepository')->byKey('social_instagram'),
                            app('A17\Twill\Repositories\SettingRepository')->byKey('social_linkedin')
                        ])
                        ->url(config('app.url'));

        $website = Schema::webSite()
                         ->identifier('#website')
                         ->url(config('app.url'))
                         ->inLanguage('nl')
                         ->name(app('A17\Twill\Repositories\SettingRepository')->byKey('meta_name'))
                         ->description(app('A17\Twill\Repositories\SettingRepository')->byKey('meta_description'));

        $webpage = Schema::webPage()
                         ->identifier('#webpage')
                         ->url(config('app.url') . '/' . $this->slug)
                         ->inLanguage('nl')
                         ->name($this->getFullSeoTitle($this->title_seo))
                         ->image($image)
                         ->description($this->description)
                         ->isPartOf($website);

        $schemas[] = $person->toScript();
        $schemas[] = $webpage->toScript();

        return $schemas;
    }

    public $mediasParams = [
        'cover' => [
            'seo' => [
                [
                    'name' => 'seo',
                    'ratio' => 0,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ]
            ],
        ],
    ];
}
