<?php

namespace App\Http\Requests\Twill;

use A17\Twill\Http\Requests\Admin\Request;
use App\Models\Redirect;
use Illuminate\Validation\Rule;

class RedirectRequest extends Request
{
    public function rulesForCreate()
    {
        return [
            'alias' => 'required|unique:App\Models\Redirect,alias',
            'title' => 'required',
            'url'   => 'required|url',
        ];
    }

    public function rulesForUpdate()
    {
        $redirect = Redirect::find($this->route('redirect'));

        return [
            'alias' => ['required', Rule::unique('redirects')->ignore($redirect->alias, 'alias')],
            'title' => 'required',
            'url'   => 'required|url',
        ];
    }
}
