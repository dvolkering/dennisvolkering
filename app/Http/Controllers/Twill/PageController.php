<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use A17\Twill\Models\Feature;
use App\Http\Controllers\RedirectController;
use App\Mail\ContactMail;
use App\Models\Album;
use App\Models\Page;
use App\Repositories\PageRepository;
use App\Rules\Recaptcha;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class PageController extends ModuleController
{
    protected $permalinkBase = '';
    protected $moduleName = 'pages';
    private $home;

    /**
     * @var PageRepository
     */
    public function init()
    {
        $this->home = Feature::where('bucket_key', 'homepage')->with('featured')->firstOrFail()->featured;
    }

    /**
     * Display homepage.
     *
     * @return View
     */
    public function home()
    {
        $this->init();
        return $this->getPageBySlug($this->home->slug, true);
    }

    /**
     * Display any other page.
     *
     * @param $slug
     *
     * @return View
     */
    public function view($slug)
    {
        $this->init();
        return $this->getPageBySlug($slug, false);
    }

    /**
     * Handle the contact form
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function contact(Request $request)
    {
        $request->validate([
            'name'               => 'required',
            'mail'               => 'required|email',
            'message'            => 'required',
            'recaptcha_response' => ['required', new Recaptcha]
        ]);

        Mail::send(new ContactMail($request));

        return back()->withSuccess('Je bericht is succesvol verstuurd, je zult zo spoedig mogelijk antwoord krijgen.');
    }


    /**
     * Get the page model by slug
     *
     * @param $slug
     * @param $isHome
     *
     * @return bool|Application|Factory|RedirectResponse|View
     */
    private function getPageBySlug($slug, bool $isHome)
    {
        $item = Page::forSlug($slug)->first();

        // Return 404 when page is not published yet
        if (is_null($item) || ! $item->published || ($isHome === false && $this->home->slug === $slug)) {
            // Check if there is a redirect configured for this slug
            // Pages always have priority above redirects
            $redirect = RedirectController::redirect($slug);

            if ($redirect === false) {
                abort(404);
            } else {
                return $redirect;
            }
        }

        return view('site.page',
            [
                'item' => $item
            ]
        );
    }
}
