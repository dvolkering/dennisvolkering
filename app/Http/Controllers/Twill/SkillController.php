<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class SkillController extends ModuleController
{
    protected $moduleName = 'skills';

    protected $indexOptions = [
        'reorder' => true,
        'publish' => false,
        'editInModal' => true,
    ];

    /*
    * Available columns of the index view
    */
    protected $indexColumns = [
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'level' => [ // field column
            'title' => 'Skill level',
            'field' => 'level',
        ],
        'type' => [ // field column
            'title' => 'Skill type',
            'field' => 'type',
        ],
        'description' => [ // field column
            'title' => 'Description',
            'field' => 'description',
        ],
    ];
}
