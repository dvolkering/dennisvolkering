<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Album;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

class AlbumController extends ModuleController
{
    protected $moduleName = 'albums';
    protected $indexOptions = [
        'reorder' => true,
    ];


    public function __construct(Application $app, Request $request)
    {
        parent::__construct($app, $request);

        $this->permalinkBase = trans('album.permalink');
        $this->indexColumns = [
        'title'     => [
            'title' => trans('album.title'),
            'field' => 'title',
        ],
        'title_seo' => [
            'title' => trans('album.title_seo'),
            'field' => 'title_seo',
        ],
        'client'    => [
            'title' => trans('album.client'),
            'field' => 'client',
        ]
    ];
    }

    /**
     * Display a album page.
     *
     * @param $slug
     *
     * @return View
     */
    public function view($slug)
    {
        $item = Album::forSlug($slug)->first();
//        $item->seoTitlePrefix = $item->getSeoPrefix();

        // Return 404 when page is not published yet
        if (is_null($item) || ! $item->published) {
            abort(404);
        }

        return view('site.album',
            [
                'item' => $item
            ]
        );
    }
}
