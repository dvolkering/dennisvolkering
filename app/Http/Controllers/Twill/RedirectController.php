<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class RedirectController extends ModuleController
{
    protected $moduleName = 'redirects';

    /*
    * Available columns of the index view
    */
    protected $indexColumns = [
        'title' => [
            'field' => 'title',
            'sort' => true,
        ],
        'alias' => [
            'title' => 'Short URL',
            'field' => 'alias',
        ],
        'url' => [
            'title' => 'URL',
            'field' => 'url',
            'sort' => true,
        ]
    ];
}
