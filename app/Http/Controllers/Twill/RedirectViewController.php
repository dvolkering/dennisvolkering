<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class RedirectViewController extends ModuleController
{
    protected $moduleName = 'redirectViews';
}
