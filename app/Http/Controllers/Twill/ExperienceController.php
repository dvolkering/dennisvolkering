<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ExperienceController extends ModuleController
{
    protected $moduleName = 'experiences';

    protected $indexOptions = [
        'reorder' => true,
        'publish' => false,
        // 'editInModal' => true, // For now, its disabled because the media won't save in the modal
    ];

    /*
    * Available columns of the index view
    */
    protected $indexColumns = [
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'role' => [ // field column
            'title' => 'Role',
            'field' => 'role',
        ],
        'date_from' => [ // field column
            'title' => 'Date from',
            'field' => 'date_from',
        ],
        'date_to' => [ // field column
            'title' => 'Date to',
            'field' => 'date_to',
        ],
        'description' => [ // field column
            'title' => 'Description',
            'field' => 'description',
        ],
    ];
}
