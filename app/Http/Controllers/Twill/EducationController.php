<?php

namespace App\Http\Controllers\Twill;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class EducationController extends ModuleController
{
    protected $moduleName = 'educations';
    protected $indexOptions = [
        'reorder'     => true,
        'publish'     => false,
        'editInModal' => true,
    ];
    /*
    * Available columns of the index view
    */
    protected $indexColumns = [
        'title'       => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'school'      => [ // field column
            'title' => 'School',
            'field' => 'school',
        ],
        'date_from'   => [ // field column
            'title' => 'Date from',
            'field' => 'date_from',
        ],
        'date_to'     => [ // field column
            'title' => 'Date to',
            'field' => 'date_to',
        ],
        'description' => [ // field column
            'title' => 'Description',
            'field' => 'description',
        ],
    ];
}
