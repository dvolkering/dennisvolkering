<?php

namespace App\Http\Controllers;

use App\Models\Redirect;
use App\Models\RedirectView;
use Illuminate\Support\Carbon;

class RedirectController extends Controller
{
    public static function redirect($slug)
    {
        $url = Redirect::where('alias', $slug)->first();

        if (is_null($url)) {
            return false;
        }

        $view = new RedirectView([
            'view' => Carbon::now()->toDateTimeString()
        ]);

        $view->redirect()->associate($url);
        $view->save();

        return redirect()->to($url->url, 302);
    }

    public function home()
    {
        return view('home');
    }
}
