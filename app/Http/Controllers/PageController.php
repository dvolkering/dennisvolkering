<?php

namespace App\Http\Controllers;

use A17\Twill\Models\Feature;
use App\Mail\ContactMail;
use App\Models\Album;
use App\Models\Page;
use App\Models\Video;
use App\Repositories\PageRepository;
use App\Rules\Recaptcha;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class PageController extends Controller
{

}
