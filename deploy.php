<?php

namespace Deployer;

require __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;

require 'vendor/skape-it/deployer-recipes/recipes/skape-it-laravel.php';
require 'vendor/skape-it/deployer-recipes/recipes/skape-it-cloudways.php';

// Get environment variables
$dotenv = Dotenv::createImmutable(__DIR__);
if (file_exists(__DIR__ . '/.env')) {
    $dotenv->load();
}

$environments = array(
    'PROD' => 'production',
    'DEV'  => 'development'
);


foreach ($environments as $key => $environment) {
    host($environment)
        ->set('hostname', env($key . '_DEPLOY_HOSTNAME'))
        ->set('identity_file', env($key . '_IDENTITY_FILE') ?? '~/.ssh/id_rsa')
        ->set('remote_user', env($key . '_DEPLOY_USER'))
        ->set('port', env($key . '_DEPLOY_PORT'))
        ->set('labels', ['stage' => $environment])
        ->set('deploy_path', env($key . '_DEPLOY_PATH'))
        ->set('cloudways_api_key', env($key . '_CLOUDWAYS_API_KEY'))
        ->set('cloudways_api_email', env($key . '_CLOUDWAYS_API_EMAIL'))
        ->set('cloudways_server_id', env($key . '_CLOUDWAYS_SERVER_ID'))
        ->set('cloudways_php_version', env($key . '_CLOUDWAYS_PHP_VERSION'))
        ->set('http_user', env($key . '_HTTP_USER') ?? null)
        ->set('writable_mode', 'chmod');
}

// Generate sitemap
after('deploy:symlink', 'artisan:sitemap:generate');

task('artisan:sitemap:generate', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan sitemap:generate');
    writeln('<info>' . $output . '</info>');
});