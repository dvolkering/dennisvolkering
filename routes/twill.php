<?php

// Register Twill routes here (eg. TwillRoutes::module('posts'))
TwillRoutes::module('articles');
TwillRoutes::module('pages');
TwillRoutes::module('redirects');
TwillRoutes::module('albums');

Route::group(['prefix' => 'about'], function () {
    TwillRoutes::module('skills');
    TwillRoutes::module('educations');
    TwillRoutes::module('experiences');
});
