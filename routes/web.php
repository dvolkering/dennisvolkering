<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Twill\PageController@home');
Route::get('/' . trans('album.permalink') . '/{slug}', 'Twill\AlbumController@view')->where('slug', '.*');
Route::get('/{slug}', 'Twill\PageController@view')->where('slug', '.*');
Route::post('/send-contact-form', 'Twill\PageController@contact');
